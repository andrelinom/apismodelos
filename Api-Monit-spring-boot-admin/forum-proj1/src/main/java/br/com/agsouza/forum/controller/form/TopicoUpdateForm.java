package br.com.agsouza.forum.controller.form;

import java.util.Optional;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.sun.istack.NotNull;

import br.com.agsouza.forum.model.Topico;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TopicoUpdateForm {
	@NotNull @NotBlank @Length(min = 3, max = 100)
	private String titulo;
	@NotNull @NotBlank @Length(min = 3, max = 100)
	private String mensagem;
	
	public Optional<Topico> update(Optional<Topico> topico) {
		if (topico.isPresent()) {
			topico.get().setTitulo(this.titulo);
			topico.get().setMensagem(this.mensagem);
		}
		return topico;		
	}
}
