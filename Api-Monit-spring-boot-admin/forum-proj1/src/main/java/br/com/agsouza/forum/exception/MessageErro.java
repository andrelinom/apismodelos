package br.com.agsouza.forum.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MessageErro {
	private String field;
	private String message;
}
