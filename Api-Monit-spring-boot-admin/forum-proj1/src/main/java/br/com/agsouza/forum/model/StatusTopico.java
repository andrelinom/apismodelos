package br.com.agsouza.forum.model;

public enum StatusTopico {
	
	NAO_RESPONDIDO,
	NAO_SOLUCIONADO,
	SOLUCIONADO,
	FECHADO;

}
