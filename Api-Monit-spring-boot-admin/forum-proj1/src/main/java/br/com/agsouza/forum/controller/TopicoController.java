package br.com.agsouza.forum.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.agsouza.forum.controller.dto.TopicoDTO;
import br.com.agsouza.forum.controller.dto.TopicoDetailsDTO;
import br.com.agsouza.forum.controller.form.TopicoForm;
import br.com.agsouza.forum.controller.form.TopicoUpdateForm;
import br.com.agsouza.forum.exception.NoCotentException;
import br.com.agsouza.forum.exception.NotFoundException;
import br.com.agsouza.forum.service.CursoService;
import br.com.agsouza.forum.service.TopicoService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
//@Api(value = "Topicos do forum")
@RequestMapping(value="/topicos")
public class TopicoController {
	@Autowired
	private TopicoService topicoService;
	@Autowired
	private CursoService cursoService;
	
	//@ApiOperation(value = "Lista de Topicos", notes = "Uma lista em stream")
	@GetMapping
	@Cacheable(value="cacheListaTopicos")
	public Flux<Page<TopicoDTO>> lista(String nomeCurso, 
			@PageableDefault(sort = "id", direction = Direction.DESC, page = 0, size = 10)  Pageable pageable) {		
		return topicoService.getLista(nomeCurso, pageable)				
				.switchIfEmpty(Flux.error(new NoCotentException()));
	}
	
	//@ApiOperation(value = "Detalhes do Topico", notes = "o topico em stream")
	@GetMapping("/{id}")
	public Mono<TopicoDetailsDTO> topico(@PathVariable Long id) {
		return topicoService.getTopico(id)
				.switchIfEmpty(Mono.error(new NotFoundException()));
	}
	
	//@ApiOperation(value = "Detalhes do Topico", notes = "o topico em stream")
	/*@GetMapping(value="/{id}")
	public Mono<ServerResponse> topicosds(@PathVariable Long id) {
		return ServerResponse.ok().body(topicoService.getTopico(id), TopicoDetailsDTO.class);
	}*/
	
	//@ApiOperation(value = "criar um Topico", notes = "Criar um novo Tópico")
	@PostMapping
	@CacheEvict(value="cacheListaTopicos", allEntries = true)
	public ResponseEntity<Mono<TopicoDetailsDTO>> topico(@RequestBody @Valid TopicoForm form, UriComponentsBuilder uriBuilder) {
		Mono<TopicoDetailsDTO> topicoDetailsDTO = topicoService
				.addTopico(form.convert(cursoService.cursoPorNome(form.getNomeCurso())));
		URI uri = uriBuilder.path("topicos/{id}").buildAndExpand(topicoDetailsDTO.blockOptional().get().getId()).toUri();
		return ResponseEntity.created(uri).body(topicoDetailsDTO);
	}
	
	//@ApiOperation(value = "Altera o Topico", notes = "o topico em stream")
	@PutMapping(value="/{id}")
	@CacheEvict(value="cacheListaTopicos", allEntries = true)
	public Mono<TopicoDetailsDTO> update(@PathVariable Long id, @RequestBody @Valid TopicoUpdateForm form) {
		return topicoService.updateTopico(id, form)
				.switchIfEmpty(Mono.error(new NotFoundException()));
	}
	
	//@ApiOperation(value = "Delete o Topico", notes = "o topico em stream")
	@DeleteMapping(value="/{id}")
	@CacheEvict(value="cacheListaTopicos", allEntries = true)
	public Mono<TopicoDetailsDTO> delete(@PathVariable Long id) {
		Mono<TopicoDetailsDTO> topicoDetails = topicoService.getTopico(id);
		return	topicoDetails
		.map(c -> {topicoService.deleteTopico(c.getId());
					return c;
				   })
				.switchIfEmpty(Mono.error(new NotFoundException()));
		
		
	}
	
}
