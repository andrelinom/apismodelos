package br.com.agsouza.forum.controller.dto;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import br.com.agsouza.forum.model.Topico;
import lombok.Getter;
import lombok.NoArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Getter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonRootName(value = "Topico")
public class TopicoDTO {
	private Long id;
	private String titulo;
	private String mensagem;
	@JsonProperty(value = "data")
	private LocalDateTime dataCriacao;
	
	public TopicoDTO(Topico topico) {
		this.id = topico.getId();
		this.titulo = topico.getTitulo();
		this.mensagem = topico.getMensagem();
		this.dataCriacao = topico.getDataCriacao();
	}

	/*
	 * public static Flux<TopicoDTO> converte(Page<Topico> topicos) { return
	 * Flux.fromStream(topicos.stream().map(TopicoDTO::new)); }
	 */
	
	public static Flux<Page<TopicoDTO>> converte(Page<Topico> topicos) {		
		return Flux.just(topicos.map(TopicoDTO::new));
	}
	
	public static Mono<TopicoDTO> converte(Optional<Topico> topico) {
		return topico.isPresent() ? Mono.just(new TopicoDTO(topico.get())) : Mono.empty();
	}
}
