package br.com.agsouza.forum.controller.form;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import lombok.Setter;

@Setter
public class LoginForm {
	private String email;
	private String senha;
	
	public UsernamePasswordAuthenticationToken converte() {
		return new UsernamePasswordAuthenticationToken(this.email, this.senha);
	}

}
