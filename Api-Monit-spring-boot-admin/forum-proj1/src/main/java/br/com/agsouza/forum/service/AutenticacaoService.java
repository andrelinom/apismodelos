package br.com.agsouza.forum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.agsouza.forum.model.Usuario;
import br.com.agsouza.forum.repository.UsuarioRepository;

@Service
public class AutenticacaoService implements UserDetailsService  {
	@Autowired
	private UsuarioRepository repository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> findByEmail = repository.findByEmail(username);
		if (findByEmail.isPresent()) {
			return findByEmail.get();
		} else {
			throw new UsernameNotFoundException("Dados inválidos");
		}
	}

}
