package br.com.agsouza.forum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.agsouza.forum.model.Topico;

public interface TopicoRepository extends JpaRepository<Topico, Long>{

	Page<Topico> findByCursoNomeContainingIgnoreCase(String nomeCurso, Pageable pagina);
	/*
	 * link com bons exemplos de jpql
	 * https://www.baeldung.com/spring-jpa-like-queries
	 * 
	 */
}
