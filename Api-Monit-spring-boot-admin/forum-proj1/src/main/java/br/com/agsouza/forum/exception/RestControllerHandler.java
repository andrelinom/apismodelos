package br.com.agsouza.forum.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestControllerHandler {
	@Autowired
	private MessageSource messageSouce;
	
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> handleNotFound(final NotFoundException ex) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoCotentException.class)
    public ResponseEntity<String> handleNoContent(final NoCotentException ex) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<String> handleBadRequest(final BadRequestException ex) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<MessageErro> handleFormat(MethodArgumentNotValidException exception) {
    	List<MessageErro> listaErros = new ArrayList<>();
    	exception.getBindingResult().getFieldErrors().forEach(c -> {
    		String message = messageSouce.getMessage(c, LocaleContextHolder.getLocale());
    		listaErros.add(new MessageErro(c.getField(), message));
    	});
    	return listaErros;
    }

}
