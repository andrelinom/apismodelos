package br.com.agsouza.forum.controller.form;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.sun.istack.NotNull;

import br.com.agsouza.forum.model.Curso;
import br.com.agsouza.forum.model.Topico;
import lombok.Getter;

@Getter
public class TopicoForm {
	@NotNull @NotBlank @Length(min = 3, max = 100)
	private String titulo;
	@NotNull @NotBlank @Length(min = 3, max = 100)
	private String mensagem;
	@NotNull @NotBlank 
	private String nomeCurso;
	
	public Topico convert(Curso curso) {
		return new Topico(this.titulo, this.mensagem, curso);
	}
}
