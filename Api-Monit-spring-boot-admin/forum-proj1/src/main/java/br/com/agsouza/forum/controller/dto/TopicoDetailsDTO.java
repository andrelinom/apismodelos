package br.com.agsouza.forum.controller.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.agsouza.forum.model.StatusTopico;
import br.com.agsouza.forum.model.Topico;
import lombok.Getter;
import reactor.core.publisher.Mono;
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TopicoDetailsDTO {
	private Long id;
	private String titulo;
	private String mensagem;
	private LocalDateTime dataCriacao;
	private String nomeAutor;
	private StatusTopico status;
	private List<RespostaDTO> respostas;
	
	
	public TopicoDetailsDTO(Optional<Topico> topico) {
		if (topico.isPresent()) {
			this.id = topico.get().getId();
			this.titulo = topico.get().getTitulo();
			this.mensagem = topico.get().getMensagem();
			this.dataCriacao = topico.get().getDataCriacao();
			if (topico.get().getAutor() != null) {
				this.nomeAutor = topico.get().getAutor().getNome(); 
			}
			this.status = topico.get().getStatus();
			if (topico.get().getRespostas() != null ) {
				topico.get().getRespostas().stream()
				.map(RespostaDTO::new)
				.collect(Collectors.toList());
			}
		}
	}


	public static Mono<TopicoDetailsDTO> converte(Optional<Topico> topico) {
		if (topico.isPresent())
			return Mono.just(new TopicoDetailsDTO(topico));
		else
			return Mono.empty();
	}
}
