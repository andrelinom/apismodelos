package br.com.agsouza.forum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agsouza.forum.model.Curso;
import br.com.agsouza.forum.repository.CursoRepository;

@Service
public class CursoService {

	@Autowired
	private CursoRepository cursoRepository;
	
	public Curso cursoPorNome(String nomeCurso) {
		return cursoRepository.findByNome(nomeCurso);		
	}
}
