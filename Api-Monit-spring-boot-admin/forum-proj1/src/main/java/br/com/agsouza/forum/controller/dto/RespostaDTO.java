package br.com.agsouza.forum.controller.dto;

import java.time.LocalDateTime;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.agsouza.forum.model.Resposta;
import br.com.agsouza.forum.model.Usuario;
import lombok.Getter;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RespostaDTO {
	private Long id;
	private String mensagem;
	private LocalDateTime dataCriacao;
	private String nomeAutor;
	
	public RespostaDTO(Resposta resposta) {
		Optional<Resposta> respostaO = Optional.of(resposta);
		if (respostaO.isPresent()) {
			this.id = respostaO.get().getId();
			this.mensagem = respostaO.get().getMensagem();
			this.dataCriacao = respostaO.get().getDataCriacao();
			Optional<Usuario> autor = Optional.ofNullable(respostaO.get().getAutor());
			if (autor.isPresent()) {
				this.nomeAutor = autor.get().getNome();
			}
		}
	}
}
