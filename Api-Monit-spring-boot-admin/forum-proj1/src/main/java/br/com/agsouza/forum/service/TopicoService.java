package br.com.agsouza.forum.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.agsouza.forum.controller.dto.TopicoDTO;
import br.com.agsouza.forum.controller.dto.TopicoDetailsDTO;
import br.com.agsouza.forum.controller.form.TopicoUpdateForm;
import br.com.agsouza.forum.model.Topico;
import br.com.agsouza.forum.repository.TopicoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TopicoService {

	@Autowired
	private TopicoRepository repository;
	
	public Flux<Page<TopicoDTO>> getLista(String nomeCurso, Pageable paginacao) {
		
		if (nomeCurso == null) {			
			return TopicoDTO.converte(repository.findAll(paginacao)); 
		} else {
			return TopicoDTO.converte(repository.findByCursoNomeContainingIgnoreCase(nomeCurso, paginacao));
		}
	}

	public Mono<TopicoDetailsDTO> getTopico(Long id) {				
		return TopicoDetailsDTO.converte(repository.findById(id));
	}
	
	@Transactional
	public Mono<TopicoDetailsDTO> addTopico(Topico topico) {
		return TopicoDetailsDTO.converte(Optional.of(repository.saveAndFlush(topico)));
	}	
	
	@Transactional
	public Mono<TopicoDetailsDTO> updateTopico(Long id, TopicoUpdateForm form) {
		Optional<Topico> topicoOptional = repository.findById(id);
		if (topicoOptional.isEmpty()) {
			return TopicoDetailsDTO.converte( form.update(topicoOptional));			
		} else {
			return Mono.empty();
		}
	}
	
	@Transactional
	public void deleteTopico(Long id) {
		repository.deleteById(id);
	}

}
